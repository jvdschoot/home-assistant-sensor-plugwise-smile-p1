**Sensor for Plugwise Smile P1 integration**

Usage:
       
      - platform: plugwisesmile
        name: Plugwise Smile
        host: x.x.x.x
        username: smile
        password: password
        resources:
          - electricity_consumed_point
          - electricity_consumed_offpeak_interval
          - electricity_consumed_peak_interval
          - electricity_consumed_offpeak_cumulative
          - electricity_consumed_peak_cumulative
          - electricity_produced_point
          - electricity_produced_offpeak_interval
          - electricity_produced_peak_interval
          - electricity_produced_offpeak_cumulative
          - electricity_produced_peak_cumulative
          - gas_consumed_interval
          - gas_consumed_cumulative
